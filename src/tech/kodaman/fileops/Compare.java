package tech.kodaman.fileops;

import java.util.List;

public class Compare {

    static boolean compareLogging = true;

    public static int compareTextSimilarities(List<String> master, List<TextIndex> masterIndex,
                                              List<String> slave, List<TextIndex> slaveIndex){

        int similarSequences = 0;
        int j = 0;
        int loopCount = 0;
        int loopEndCount = 0;
        Character currCharMaster = 'a';

        for (int i = 0; i <= master.size(); i++) {

            // To set the current Character we need to check i in regards to start end index
            // For example if i = 9, and b, 4, 29, then the currChar would be b.
            //Character currCharMaster = masterIndex.get(i).getLetter();
            System.out.println("Loop i = " + i);

            // Set curr letter on first iteration
            if(i == 0)
            {
                currCharMaster = masterIndex.get(i).getLetter();
                loopEndCount = (slaveIndex.get(j).getEnd() - slaveIndex.get(j).getStart());
            }

            // Only change the loopEndCount when a new letter is found.
            if(currCharMaster != masterIndex.get(i).getLetter())
            {
                loopEndCount = (slaveIndex.get(j).getEnd() - slaveIndex.get(j).getStart());
                currCharMaster = masterIndex.get(i).getLetter();
                j++;
            }

            do{

                // Compare strings
                if(master.get(i).equals(slave.get(j)))
                {
                    similarSequences++;
                }

                loopCount++;

            }while (loopCount != loopEndCount);

            //j++;
            loopCount = 0;
        }

        return similarSequences;
    }

    public static int compareTextSimilarities2(List<String> master, List<TextIndex> masterIndex,
                                              List<String> slave, List<TextIndex> slaveIndex){

        // Local variables
        int sameSequence = 0;
        Character masterCurrChar = 'a';
        Character slaveCurrChar = 'a';
        String masterCurrString = "";
        String slaveCurrString = "";
        int masterloopCount = 0;
        int masterloopEndCount = 0;
        int masterCharRange = 0;
        int slaveloopCount = 0;
        int slaveloopEndCount = 0;
        int slaveCharRange = 0;
        boolean hasLetter = false;
        int j = 0;
        int k = 0;
        int g = 0;
        int m = 0;

        outerloop:
        for(int i = 0; i < masterIndex.size(); i++) {

            // assign current letter, and the range
            masterCurrChar = masterIndex.get(i).getLetter();
            masterCharRange = masterIndex.get(i).getEnd() - masterIndex.get(i).getStart() +1;

            // check if letter exists
            // if it does set slave range
            // FIXME: When no letter match is found!
            for(j = 0; j < slaveIndex.size(); j++)
            {
                if(masterCurrChar.equals(slaveIndex.get(j).getLetter()))
                {
                    slaveCharRange = slaveIndex.get(j).getEnd() - slaveIndex.get(j).getStart() +1;
                    break;
                } else if(j == slaveIndex.size()-1 && !hasLetter){

                    // if the letter does not exist continue to the next letter.
                    continue outerloop;
                }
            }


            // Now we know both ranges, master and slave
            // We can compare them without doing any useless comparisons
            // The goal here is to take the current master character, i.e. a, 5, 13
            // and compare each of those to the range of slave list, i.e. a, 6, 23
            // FIXME: The start and end needs to be watched!
            for(k = masterIndex.get(i).getStart(); k < masterIndex.get(i).getEnd(); k++)
            {
                // set the master file string here
                masterCurrString = master.get(k);

                for(m = slaveIndex.get(j).getStart(); m < slaveIndex.get(j).getEnd(); m++)
                {
                    // set the slave file string here
                    slaveCurrString = slave.get(m);

                    // compare for equality, and if it is add 1 to the sameSequence variable
                    if(slaveCurrString.equals(masterCurrString))
                    {
                        sameSequence++;
                    }
                }
            }

        }
        return sameSequence;
    }

    public int compareTextFiles(List<String> textA, List<String> textB)
    {
        int sameSequence = 0;

        for (int i = 0; i < textA.size(); i++) {
            for (int j = 0; j < textB.size(); j++) {

                if(textA.get(i).equals(textB.get(j)))
                {
                    sameSequence++;
                }
            }
        }

        return sameSequence;
    }

    public static int compareTextFiles2(List<String> master, List<TextIndex> masterIndex,
                                 List<String> slave, List<TextIndex> slaveIndex)
    {
        //BUG should have found 6 same word sequences i >= slaveIndex.size() is not a good way to do it

        Character masterChar = 'a';
        Character slaveChar = 'a';
        int sameSequence = 0;
        int comparisonsCount = 0;

        for (int i = 0; i < masterIndex.size(); i++) {

            if(slaveIndex.size() <= comparisonsCount)
            {
                break;
            }

            masterChar = masterIndex.get(i).getLetter();
            int slaveTempIndex = File.charIndex(masterChar, slaveIndex);

            if( slaveTempIndex >= 0)
            {
                slaveChar = slaveIndex.get(slaveTempIndex).getLetter();
                int offset = 0;
                if(masterIndex.get(i).getStart() == masterIndex.get(i).getEnd()){offset = 1;}
                for (int j = masterIndex.get(i).getStart(); j <= masterIndex.get(i).getEnd() + offset; j++) {

                    System.out.println("i = " + i + " j= " + j);
                    int offset2 = 0;

                    if(slaveIndex.get(i).getStart() == slaveIndex.get(i).getEnd()){offset2 = 1;}
                    for (int k = slaveIndex.get(slaveTempIndex).getStart(); k <= slaveIndex.get(slaveTempIndex).getEnd() + offset2; k++) {

                        System.out.println("k = " + k);

                        String masterStr = master.get(j);
                        String slaveStr = slave.get(k);



                        if(master.get(j).equals(slave.get(k)))
                        {
                            sameSequence++;
                        }

                        comparisonsCount++;

                    }
                }

            } else {
                continue;
            }
        }


        return sameSequence;
    }

    public static int compareTextFiles3(List<String> master, List<TextIndex> masterIndex,
                                        List<String> slave, List<TextIndex> slaveIndex)
    {

        List<TextIndex> newMasterIndex = File.definiteComparisonsIndex(masterIndex, slaveIndex);
        int howManyOnMaster = 0;
        int howManyOnSlave = 0;
        int sameSequence = 0;

        File.createLogFile("/Users/terminalX/Documents/Projects/Catch Plagiarists/sm_doc_set/");


        for (int i = 0; i < newMasterIndex.size(); i++)
        {

            for (int j = newMasterIndex.get(i).getStart(); j <= newMasterIndex.get(i).getEnd(); j++) {

                // Find index on slave
                int slaveTempIndex = File.charIndex(newMasterIndex.get(i).getLetter(), slaveIndex);

                int offset = 0;
                if(slaveIndex.get(slaveTempIndex).getStart() == slaveIndex.get(slaveTempIndex).getEnd()){offset = 1;}

                for (int k = slaveIndex.get(slaveTempIndex).getStart(); k < slaveIndex.get(slaveTempIndex).getEnd() + offset; k++)
                {
                    String strTempMaster = master.get(j);
                    String strTempSlave = slave.get(k);

                    if(master.get(j).equals(slave.get(k)))
                    {
                        sameSequence++;

                        if(compareLogging){
                            File.logToFile("Master: " + master.get(j)+ " -- Slave: " + slave.get(k)
                                    + " " +sameSequence);
                        }
//                        System.out.println("Master: " + master.get(j)+ " -- Slave: " + slave.get(k)
//                        + " " +sameSequence);
                    } else{

                        if(compareLogging){
                            File.logToFile("Not the same = " + "Master: " + master.get(j)+ " -- Slave: " + slave.get(k));
                        }

                        // System.out.println("Not the same = " + "Master: " + master.get(j)+ " -- Slave: " + slave.get(k));
                    }
                }

            }
        }

        File.closeFile();

        return sameSequence;
    }

    public static int compareTextFilesLD(List<String> master, List<TextIndex> masterIndex,
                                  List<String> slave, List<TextIndex> slaveIndex)
    {
        // Use Leveinshtein Distance algorithm
        return 0;
    }
}
