package tech.kodaman.fileops;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class File{

    static String folderPath = "";
    static boolean debug = false;
    //static String folderPathSmallSet = "/Users/terminalX/Documents/Projects/Catch Plagiarists/sm_doc_set/";
    static PrintWriter writer;

    public static java.io.File[] getFileNames(String path){

        java.io.File folder = new java.io.File(path);
        java.io.File[] listOfFiles = folder.listFiles();

        if (debug) {
            for(java.io.File file : listOfFiles){
                if(file.isFile()){
                    System.out.println(file.getName());
                }
            }
        }

        return listOfFiles;
    }

    public static List<String> getWordSequence(String fileName, int numberOfWords,
                                               boolean sort, boolean shifted) throws FileNotFoundException {

        List<String> tempWordSequence = new ArrayList<String>();
        List<String> wordSequenceOut = new ArrayList<String>();
        int wordCount = 0;
        int i = 0;
        StringBuilder sb = new StringBuilder();

        //java.io.File textFile = new java.io.File(folderPath + fileName + ".txt");
        //Scanner inputFile = new Scanner(textFile, "utf-8");

        // BUG: Punctuation symbols like ",', and others are being turned to question marks.
        Scanner inputFile = new Scanner(new FileReader(folderPath + fileName + ".txt"));

        tempWordSequence = getSingleWords(inputFile);

        if(shifted)
        {
            wordSequenceOut = getWordSequenceShifted(tempWordSequence, numberOfWords);
        } else {
            wordSequenceOut = getWordSequence(tempWordSequence, numberOfWords);
        }


        //tempWordSequence.add(sb.toString()); // Add remaining words

        // Check if it needs to be sorted
        if(sort)
        {
            sortWordSequence(wordSequenceOut);
        }

        return wordSequenceOut;
    }

    private static List<String> getSingleWords(Scanner inputFile)
    {
        List<String> wordSequence = new ArrayList<String>();
        int testCount = 0;
        while(inputFile.hasNext())
        {
            String tempString = inputFile.next();
            tempString = tempString.replaceAll("[^$()&A-Za-z0-9]", "");
            String tempString2 = "";

            if(debug)
            {
                System.out.println(tempString + " " +testCount);
                testCount++;
            }


            if(tempString.length() == 0)
            {
                continue;
            }

            // remove spaces at the beginning of the string
            if(tempString.charAt(0) == ' ')
            {
                tempString2 = removeSpacesInTheBeginning(tempString);
                wordSequence.add(tempString2);
                continue;
            } else
            {
                wordSequence.add(tempString);
            }

            //*********************************************

            //wordSequence.add(inputFile.next());
        }

        return wordSequence;
    }

    // This method creates a word sequence
    // index 0 : words 1-6 (1, 2, 3, 4, 5, 6)
    // index 1 : words 7-12.. and so on
    // No shifting going on here!
    public static List<String> getWordSequence(List<String> wordSequenceIn, int numOfWords) {

        List<String> wordSequenceOut = new ArrayList<String>();
        StringBuilder stringTemp = new StringBuilder();
        int numOfSentences = 0;
        List<String> tempList;

        // takes single word let's say 598 words

        // find the number of complete sentences
        // i.e. 598 / 7 = 85, remaining 3
        // TODO might need to add a space or remove space from singleWords method and remove the space from shiftedMethod?
        numOfSentences = wordSequenceIn.size() / numOfWords;

        // add to wordSequenceOut
        for (int i = 0; i <wordSequenceIn.size() - numOfWords; i = i + numOfWords) {
            tempList = new ArrayList<String>(wordSequenceIn.subList(i, i + numOfWords));

            // join
            wordSequenceOut.add(String.join(" ", tempList));
            tempList.clear();
        }

        // add remaining


        return wordSequenceOut;
    }

    private static List<String> getWordSequenceShifted(List<String> wordSequenceIn, int numOfWords)
    {
        List<String> wordSequenceOut = new ArrayList<String>();

        // index[0] is going to be first X words, ie words 0 to 5
        // if the step is 1, then index[1] will be the next X words
        // ie 1 to 6, next 2 to 7, next 3 to 8, etc...

        StringBuilder stringTemp = new StringBuilder();
        int wordCount = 0;
        int j = 0;
        boolean indexCompleted = false;

        for(int i = 0; i < wordSequenceIn.size() - (numOfWords - 1); i++)
        {
            while(!indexCompleted)
            {
                wordCount++;

                if(wordCount == numOfWords)
                {
                    stringTemp.append(wordSequenceIn.get(i + j));
                } else {
                    stringTemp.append(wordSequenceIn.get(i + j) + " ");
                }


                if (wordCount == numOfWords) {
                    wordSequenceOut.add(i, stringTemp.toString());
                    //if(debugInfo)System.out.println("wSequence " + wSequenceOut.get(i));
                    stringTemp.delete(0, stringTemp.length());
                    wordCount = 0;
                    // i++;
                    j = 0;
                    indexCompleted = true;
                }
                j++;
            }
            j = 0;
            indexCompleted = false;

        }
        return wordSequenceOut;
    }

    // Index methods
    public static List<TextIndex> getFileIndex(List<String> sortedWordSequence)
    {

        List<TextIndex> textIndices = new ArrayList<>();
        Character currChar = 'a';
        int start = 0;
        int end = 0;

        for(int i = 0; i < sortedWordSequence.size(); i++)
        {

            // find the first index char
            if(i == 0)
            {
                currChar = sortedWordSequence.get(i).charAt(0);
                Character.toLowerCase(currChar);
                start = i;
                continue;
            }

            // compare if is a new char or no more new letters
            if(currChar != Character.toLowerCase(sortedWordSequence.get(i).charAt(0))
                    || i == sortedWordSequence.size() -1)
            {
                // Break if last index
                if(i == sortedWordSequence.size() -1)
                {
                    Character tempChar = Character.toLowerCase(sortedWordSequence.get(i).charAt(0));

                    // Add current letter if last letter is not the same
                    if(currChar != Character.toLowerCase(sortedWordSequence.get(i).charAt(0)))
                    {
                        textIndices.add(new TextIndex(currChar, start, i -1));
                        currChar = Character.toLowerCase(sortedWordSequence.get(i).charAt(0));
                        textIndices.add(new TextIndex(currChar, i, i));
                        break;
                    }

                    // Add last letter
                    currChar = Character.toLowerCase(sortedWordSequence.get(i).charAt(0));

                    textIndices.add(new TextIndex(currChar, start, i));
                    break;
                }

                textIndices.add(new TextIndex(currChar, start, i -1));
                currChar = Character.toLowerCase(sortedWordSequence.get(i).charAt(0));

                start = i;

            }

            // if is the same keep counting index
//            if(currChar.equals(sortedWordSequence.get(i).charAt(0)))
//            {
//                end++;
//            }
        }

        return textIndices;
    }

//    public List<CharIndex> getCharIndex(List<String> sortedWordSequence)
//    {
//        return null;
//    }

//    /**
//     * This method will take a textIndexList, and check the size for each
//     * letter. i.e. If letter a starts at 0, and ends at 8. a = 9
//     * @param textIndexList
//     * @return charIndices
//     */
//    public List<LetterIndex> getCharIndex(List<TextIndex> textIndexList){
//
//        List<LetterIndex> charIndices = new ArrayList<>();
//
//        for (int i = 0; i < textIndexList.size(); i++) {
//
//            charIndices.add(new LetterIndex())
//        }
//
//        return charIndices;
//    }

    // Helper methods
    // TODO need to test this method!
    public static List<TextIndex> definiteComparisonsIndex(List<TextIndex> masterIndex, List<TextIndex> slaveIndex)
    {
        List<TextIndex> newIndex = new ArrayList<>();
        int possibleComparisons = 0;
        int howManyOnMaster = 0;
        int howManyOnSlave = 0;
        int charIndex = 0;
        Character masterTempChar;
        Character slaveTempChar;

        for (int i = 0; i < masterIndex.size(); i++)
        {
            //howManyOnMaster = howManyOnCurrentLetter(masterIndex.get(i));
            //masterTempChar = masterIndex.get(i).getLetter();

            // Does it exist in slave?
            charIndex = charIndex(masterIndex.get(i).getLetter(), slaveIndex);

            if(charIndex >= 0)
            {
                newIndex.add(masterIndex.get(i));
                //slaveTempChar = slaveIndex.get(charIndex).getLetter();
                //howManyOnSlave = howManyOnCurrentLetter(slaveIndex.get(charIndex));
                //possibleComparisons += (howManyOnMaster * howManyOnSlave);
            } else{
                continue;
            }

        }

        return newIndex;
    }

    public static int howManyOnCurrentLetter(TextIndex textIndex)
    {

        int howMany = 0;

        // Example: a, start: 0, end: 0
        // Example: b, start: 1, end: 5
        // Example: c, start: 6, end: 6


        if(textIndex.getStart() ==  textIndex.getEnd())
        {
            howMany = 1;
        } if (textIndex.getStart() == 0 && textIndex.getEnd() != 0){

            howMany = (textIndex.getEnd() + 1) - (textIndex.getStart() + 1);
            howMany += 1; // Adding 1 because even though arrays start at 0 we will 1 digit on every check here
        } else{
        howMany = (textIndex.getEnd()) - (textIndex.getStart());
        howMany += 1;
        }


        return howMany;
    }

    public static int charIndex(Character currLetter, List<TextIndex> fileIndex)
    {
        for (int i = 0; i < fileIndex.size(); i++) {
            if(currLetter.equals(fileIndex.get(i).getLetter()))
            {
                return i;
            }
        }

        return -1;
    }

    public static void printList(List<String> wordSequence)
    {
        for(String object : wordSequence)
        {
            System.out.println(object);
        }
    }

    // TODO add a logging class later or known logging library
    public static void createLogFile(String path)
    {
        try{
            writer = new PrintWriter(path + "log.txt");
        } catch(FileNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    public static void logToFile(String str)
    {

        // Check if file exists if not create it
        writer.println(str);
    }

    public static void closeFile()
    {
        writer.close();
    }

    private static List<String> sortWordSequence(List<String> wordSequence)
    {
        Collections.sort(wordSequence, String.CASE_INSENSITIVE_ORDER);

        return wordSequence;
    }

    private static String removeSpacesInTheBeginning(String inputStr)
    {
        StringBuilder sb = new StringBuilder(inputStr);

        while(sb.charAt(0) == ' ')
        {
            sb.deleteCharAt(0);
        }

        return sb.toString();
    }

    // Getters and Setters
    public static String getFolderPath() {
        return folderPath;
    }

    public static void setFolderPath(String folderPath) {
        File.folderPath = folderPath;
    }

}
