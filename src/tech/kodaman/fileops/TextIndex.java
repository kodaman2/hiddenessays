package tech.kodaman.fileops;


public class TextIndex {

    private Character letter;
    private int start;
    private int end;

    public TextIndex(Character letter, int start, int end)
    {
        this.letter = letter;
        this.start = start;
        this.end = end;
    }


    @Override
    public String toString() {
        return "TextIndex{" +
                "letter = " + letter +
                ", start = " + start +
                ", end = " + end +
                '}';
    }

    public Character getLetter() {
        return letter;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }
}
