import org.junit.Assert;
import org.junit.Test;
import tech.kodaman.fileops.Compare;
import tech.kodaman.fileops.File;
import tech.kodaman.fileops.TextIndex;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;


public class CompareTest {

    String folderPathSmallSet = "/Users/terminalX/Documents/Projects/Catch Plagiarists/sm_doc_set/";

    @Test
    public void compareTest01() throws FileNotFoundException {
        List<String> myWordSequence = new ArrayList<String>();
        List<TextIndex> myTextIndices = new ArrayList<>();

        List<String> myWordSequence2 = new ArrayList<String>();
        List<TextIndex> myTextIndex2 = new ArrayList<>();

        File.setFolderPath(folderPathSmallSet);

        myWordSequence = File.getWordSequence("abf0704", 7, true, true);
        myTextIndices = File.getFileIndex(myWordSequence);

        myWordSequence2 = File.getWordSequence("abf0704 copy", 7, true, false);
        myTextIndex2 = File.getFileIndex(myWordSequence2);

        for(int i = 0; i < myTextIndices.size(); i++)
        {
            System.out.println(myTextIndices.get(i).toString());
        }

        File.printList(myWordSequence);
        System.out.println("Word sequence " + myWordSequence.size());

        int sequenceCount;
        sequenceCount = Compare.compareTextFiles3(myWordSequence, myTextIndices,
                myWordSequence2, myTextIndex2);
        System.out.println("Similar sentences " + sequenceCount);
    }

    @Test
    public void compareTest02() throws FileNotFoundException {
        List<String> myWordSequence = new ArrayList<String>();
        List<TextIndex> myTextIndices = new ArrayList<>();

        List<String> myWordSequence2 = new ArrayList<String>();
        List<TextIndex> myTextIndex2 = new ArrayList<>();

        File.setFolderPath(folderPathSmallSet);

        myWordSequence = File.getWordSequence("fab001", 7, true, true);
        myTextIndices = File.getFileIndex(myWordSequence);

        myWordSequence2 = File.getWordSequence("fab001 copy", 7, true, false);
        myTextIndex2 = File.getFileIndex(myWordSequence2);

        // NOTE: Remove after testing
        //File.calculatePossibleComparisons(myTextIndices, myTextIndex2);

        // print text 1 file stuff
        for(int i = 0; i < myTextIndices.size(); i++)
        {
            System.out.println(myTextIndices.get(i).toString());
        }

        File.printList(myWordSequence);
        System.out.println("Word sequence 1 " + myWordSequence.size());
        //-----------------------------------

        // print text 1 file stuff
        for(int i = 0; i < myTextIndex2.size(); i++)
        {
            System.out.println(myTextIndex2.get(i).toString());
        }

        File.printList(myWordSequence2);
        System.out.println("Word sequence 2 " + myWordSequence2.size());
        //-----------------------------------

        int sequenceCount;
        sequenceCount = Compare.compareTextFiles3(myWordSequence, myTextIndices,
                myWordSequence2, myTextIndex2);

        System.out.println("Similar sentences " + sequenceCount);

        Assert.assertEquals(5, sequenceCount);
    }

}