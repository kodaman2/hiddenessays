import org.junit.Assert;
import org.junit.Test;
import tech.kodaman.fileops.File;
import tech.kodaman.fileops.TextIndex;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;


public class FileTest {

    String folderPathSmallSet = "/Users/terminalX/Documents/Projects/Catch Plagiarists/sm_doc_set/";

    @Test
    public void testFileNamesSmDocSet()
    {
        java.io.File[] fileNames = File.getFileNames(folderPathSmallSet);
        Assert.assertEquals(27, fileNames.length);
    }

    @Test
    public void testWordSequenceShortText01() throws FileNotFoundException {
        List<String> myWordSequence = new ArrayList<String>();
        File.setFolderPath(folderPathSmallSet);

        myWordSequence = File.getWordSequence("ehc229", 9, false, true);
        File.printList(myWordSequence);
        System.out.println(myWordSequence.size());

    }

    @Test
    public void testFileIndexShortText01() throws FileNotFoundException {
        List<String> myWordSequence = new ArrayList<String>();
        List<TextIndex> myTextIndices = new ArrayList<>();
        File.setFolderPath(folderPathSmallSet);

        myWordSequence = File.getWordSequence("ehc229", 9, true, true);
        myTextIndices = File.getFileIndex(myWordSequence);

        for(int i = 0; i < myTextIndices.size(); i++)
        {
            System.out.println(myTextIndices.get(i).toString());
        }

        File.printList(myWordSequence);
        System.out.println(myWordSequence.size());

    }

    @Test
    public void testWordSequenceLongText01() throws FileNotFoundException {
        List<String> myWordSequence = new ArrayList<>();
        File.setFolderPath(folderPathSmallSet);

        myWordSequence = File.getWordSequence("abf0704", 7, false, true);
        File.printList(myWordSequence);
        System.out.println(myWordSequence.size());

    }

    @Test
    public void testFileIndexLongText01() throws FileNotFoundException {
        List<String> myWordSequence = new ArrayList<String>();
        List<TextIndex> myTextIndices = new ArrayList<>();
        File.setFolderPath(folderPathSmallSet);

        myWordSequence = File.getWordSequence("abf0704", 7, true, true);
        myTextIndices = File.getFileIndex(myWordSequence);

        for(int i = 0; i < myTextIndices.size(); i++)
        {
            System.out.println(myTextIndices.get(i).toString());
        }

        File.printList(myWordSequence);
        System.out.println(myWordSequence.size());

    }

}